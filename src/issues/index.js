var axios = require('axios');
var parseLinkHeader = require('parse-link-header');
var _ = require('lodash');

axios.defaults.headers.common['Authorization'] = 'token 7f3b6270e96c10c7cb2a4a465fb13f1426d2ee59';
axios.defaults.headers.common['User-Agent'] = 'test';
axios.interceptors.response.use(function(response){
  return response;
}, function (err) {
  console.log(err);
});

var PER_PAGE = 100;
var CONCURRENCY = 60;

function getIssues(res) {
  var repo_url = 'https://api.github.com/orgs/att/repos';
  var repos = [];
  var issues = [];
  var comments = [];
  getRequestTotalPages(repo_url)// get number of pages for repos
    .then(function(pages){// get all repos
      return hanldeArrayOfPromises(generatePagedRequests(repo_url, pages));
    }, function(err) {
      console.log(err);
    })
    .then(function() {// get number of pages for issues
      repos = _.flatten(parseResponse(arguments));
      return getAttributesPage(repos,'/issues');
    }, function(err) {
      console.log(err);
    })
    .then(function() {// get all issues    
      var repoIssuesPageArray = _.flatten(Array.prototype.slice.call(arguments));
      return getAttributes(repos, repoIssuesPageArray, '/issues');
    }, function(err) {
      console.log(err);
    })
    .then(function () { // get number of pages for comments
      issues = _.flatten(parseResponse(arguments));
      return getAttributesPage(repos,'/issues/comments');
    }, function(err) {
      console.log(err);
    })
    .then(function() {// get all comments    
      var repoCommentsPageArray = _.flatten(Array.prototype.slice.call(arguments));
      return getAttributes(repos, repoCommentsPageArray, '/issues/comments');
    },function(err){
      console.log(err);
    })
    .then(function(){//mapping comments to issues
      comments = _.flatten(parseResponse(arguments));
      issues = issues.map(function(issue){
        var newComments=[];
        if(issue.comments){          
          comments.forEach(function(comment){
            if(comment.issue_url==issue.url){
              newComments.push(comment);
            }
          });
        }
        return {
          id: issue.id,
          url: issue.url,
          title: issue.title,
          user:issue.user,
          body:issue.body,
          comments: newComments
        }
      }); 
      res.send(issues);
    });
}

function getAttributesPage(repos,attribute_url){
  var promises = [];
  repos.forEach(function(repo) {
      if(repo.open_issues_count){
        var newUrl = 'https://api.github.com/repos/att/' + repo.name + attribute_url;
        promises.push(getRequestTotalPages(newUrl));
      } 
  })
  return hanldeArrayOfPromises(promises);
}

function getAttributes(repos,repoPageArray, attribute_url){
  var promises = [];
  var i=0;
  repos.forEach(function (repo) {
    if(repo.open_issues_count){
      var newUrl = 'https://api.github.com/repos/att/' + repo.name + attribute_url;
      promises.push(generatePagedRequests(newUrl, repoPageArray[i]));
      i+=1;
    }
  });
  promises = _.flatten(promises);
  return hanldeArrayOfPromises(promises);
}

function generatePagedRequests(url, pages) {
  return _.range(pages).map(function(i){
    return axios.get(url + '?page=' + (i+1)+'&per_page='+PER_PAGE);
  });
}

function parseResponse(response){
  return _.flatten(Array.prototype.slice.call(response)).map(function(res){
    return res ? res.data : [];
  });
}

function getRequestTotalPages(url){
  var promise = new Promise(function(resolve, reject){
    axios.get(url+'?per_page='+PER_PAGE)
      .then(function(res) {
        if(res && res.headers) {
          var links = parseLinkHeader(res.headers.link);
          var pages = links ? (parseInt(links.last.page) || 1) : 1;
          resolve(pages);
        }else{
          resolve(1);
        }
      });
  });
  return promise;
}

function hanldeArrayOfPromises(promises){//limits the concurrency requests to 60
  if(promises.length <= CONCURRENCY){
    return axios.all(promises);
  } else{
    var promise = new Promise(function(resolve, reject) {
      handlePromises(promises, 0, resolve, []);
    });
    return promise;
  }
}

function handlePromises(promises, handled, resolve, result){
  if(promises.length === handled){
    resolve(result);
    return;
  }
  const end = Math.min(handled + CONCURRENCY, promises.length);
  axios.all(promises.slice(handled, end))
    .then(function (res) {
      result = result.concat(res);
      handlePromises(promises, end, resolve, result);
    });
}

module.exports = {
  getIssues: getIssues
};
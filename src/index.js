var express = require('express');
var issues = require('./issues/index');
var app = express();

app.get("/issues", function(req, res, next) {
  issues.getIssues(res);
});

app.listen(process.env.PORT || 3000);
console.log('started.');